-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 26 fév. 2020 à 16:05
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;

--
-- Base de données :  projetwebmap
--

-- --------------------------------------------------------

--
-- Structure de la table groupe
--

DROP TABLE IF EXISTS groupe;
CREATE TABLE IF NOT EXISTS groupe(
   IdGrp INTEGER AUTO_INCREMENT,
   nomGrp VARCHAR(60) NOT NULL,
   IdUserResponsable INTEGER NOT NULL,
   IdEve CHAR(6) NOT NULL,
   nomEve CHAR(80) NOT NULL,
   dateEve DATE NOT NULL,
   adresseEve CHAR(80) NOT NULL,
   PRIMARY KEY(IdGrp), 
   FOREIGN KEY(IdUserResponsable) REFERENCES utilisateur(IdUser)
);

INSERT INTO groupe (IdGrp, nomGrp, IdUserResponsable, IdEve, nomEve, dateEve, adresseEve) VALUES
(1, 'nom de Groupe', 1, '102449', 'Paris joli', '2020-05-28', 'Avenue Winston Churchill'),
(2, 'nom de Groupe n2', 3, '94490', 'Soccer Mommy + Guest', '2020-06-15', '7 port de la Gare'),
(3, 'Groupe de lecture', 1, '70187', 'L\'Étagère du Oh!', '2020-03-24', '1 place Jules Joffrin, Paris'),
(4, 'Groupe d\'écoute musical', 1, '101905', 'INSTALLATION SONORE - Orpheus Collective de Violaine Lochu', '2020-05-27', 'Face au 61 quai de la Seine, Paris'),
(5, 'British', 1, '88823', 'Margotines en anglais', '2020-04-25', '41, rue d Alleray, Paris'),
(6, 'Pas trop tôt', 3, '98945', 'Atelier console rétro', '2020-03-28', '88 Ter boulevard de Port-Royal, Paris'),
(7, 'Pour la peinture', 3, '101775', 'ARTIST TALK : REUBEN NEGRON', '2020-03-24', '34 avenue de New York, Paris'),
(8, 'Groupe Violon', 6, '98008', 'Des violoncellistes dans tous leurs états', '2020-03-28', '123 rue Saint-Jacques, Paris'),
(9, 'La visite', 6, '85703', 'Visite guidée : A toutes les femmes la patrie reconnaissante', '2020-03-24', '4  rue marie stuart, Paris');
COMMIT;
--
-- Déchargement des données de la table groupe
--

--
-- Structure de la table utilisateur
--

DROP TABLE IF EXISTS utilisateur;
CREATE TABLE IF NOT EXISTS utilisateur(
   IdUser INTEGER AUTO_INCREMENT,
   nomUtilisateur VARCHAR(60) NOT NULL UNIQUE,
   motDePasse VARCHAR(60) NOT NULL,
   PRIMARY KEY(IdUser) 
);


--
-- Déchargement des données de la table utilisateur
--

INSERT INTO utilisateur (IdUser, nomUtilisateur, motDePasse) VALUES
(1, 'test', 'test' ),
(2, 'anthony', 'tess'),
(3, 'emkyu', 'mq'),
(4, 'uhoh', 'stinky'),
(5,'Vadimador', 'vvv'),
(6, 'Démo1', '123'),
(7, 'Démo2', '123');
COMMIT;
--
-- Structure de la table liaison
--

DROP TABLE IF EXISTS assGrpUser;
CREATE TABLE IF NOT EXISTS assGrpUser(
   IdGrp INTEGER,
   IdUser INTEGER,
   InvStatut  enum('ATTENTE','ACCEPTER') NOT NULL,
   PRIMARY KEY(IdGrp, IdUser),
   FOREIGN KEY(IdGrp) REFERENCES groupe(IdGrp),
   FOREIGN KEY(IdUser) REFERENCES utilisateur(IdUser)
);

INSERT INTO assGrpUser(IdGrp, IdUser, InvStatut) VALUES
(1,1, 'ACCEPTER'),
(1,3, 'ACCEPTER'),
(2,1, 'ACCEPTER'),
(2,2, 'ACCEPTER'),
(1,5, 'ATTENTE'),
(2,5, 'ATTENTE'),
(2,3, 'ACCEPTER'),
(3, 3, 'ATTENTE'),
(3, 6, 'ATTENTE'),
(3, 7, 'ATTENTE'),
(4, 1, 'ACCEPTER'),
(5, 1, 'ACCEPTER'),
(5, 6, 'ATTENTE'),
(5, 7, 'ATTENTE'),
(5, 2, 'ATTENTE'),
(5, 3, 'ATTENTE'),
(6, 3, 'ACCEPTER'),
(6, 1, 'ATTENTE'),
(6, 6, 'ATTENTE'),
(6, 7, 'ATTENTE'),
(7, 3, 'ACCEPTER'),
(7, 5, 'ATTENTE'),
(7, 6, 'ATTENTE'),
(7, 1, 'ATTENTE'),
(7, 2, 'ATTENTE'),
(8, 6, 'ACCEPTER'),
(9, 6, 'ACCEPTER'),
(8, 7, 'ATTENTE'),
(8, 2, 'ATTENTE'),
(9, 7, 'ATTENTE');
COMMIT;




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
