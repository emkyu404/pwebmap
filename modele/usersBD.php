<?php 
/*Fonctions-modèle réalisant la gestion d'une table de la base,
** ou par extension gérant un ensemble de tables. 
** Les appels à ces fcts se fp,t dans c1.
*/

//require ("modele/connect.php") ; //connexion MYSQL et sélection de la base, $link défini

function verif_ident_BD($login,$mdp){
	require ("modele/connect.php") ; 

	$sql="SELECT * FROM utilisateur where nomUtilisateur=:login";
	$resultat= array();
	$_SESSION['login'] = "" ; 

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':login', $login);
		$bool = $commande->execute();

		if ($bool) {
			$resultat = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements

			if(empty($resultat)){
				return false;
			}
			else{
				if ($resultat[0]['motDePasse'] == $mdp) {
					$_SESSION['login'] = $login;
					$_SESSION['ID'] = $resultat[0]['IdUser'];
					return true;
				} else {
					return false;
				}
			}
			
		}
	}

	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}

	return false;
}

function verif_inscriptionBD($login,$mdp){
	require ("modele/connect.php") ; 

	$sql="SELECT * FROM utilisateur";
	$resultat= array(); 

	try {
		$commande = $pdo->prepare($sql);
		$bool = $commande->execute();

		if ($bool) {
			$resultat = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements

			for ($i = 0; $i < count($resultat); $i++){
				if($resultat[$i]['nomUtilisateur'] == $login && $resultat[$i]['motDePasse'] == $mdp){
					return false;
				}
			}
			return true;
		}
	}

	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
}

function inscrireBD($login,$mdp) {
	require ("modele/connect.php") ;  

	//$sql2='SELECT ID FROM utilisateur ORDER BY ID DESC LIMIT 1';
	//$commandeID = $pdo->prepare($sql2);
	//$boolID = $commandeID->execute();

	//if($boolID){
	//	$id = $commandeID->fetchAll(PDO::FETCH_ASSOC);
	//}

	$sql='INSERT INTO utilisateur (nomUtilisateur, motDePasse) VALUES(? , ?)';

	try {
		$commande = $pdo->prepare($sql);
		$bool = $commande->execute(array($login , $mdp));
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
}
  
function InviterUtilisateur($tabInvite) { // pour chaque utilisateur créer un lien entre le groupe et lui
	$lastid = getLastIdGroupe();
	for ($i=0; $i < count($tabInvite); $i++) { 
		$idu = getUserIdFromName($tabInvite[$i]);
		if(empty($idu) == false) {
			créerLien($lastid[0]['MAX(IdGrp)'], $idu[0]['IdUser'], 'ATTENTE');
		}
	}
	$url = "index.php?controle=users&action=accueil";
	header ("Location:" .$url);
}

function créerGroupe($NomGroupe, $idUserResponsable, $idEve, $NomEve, $dateEve, $adresseEve){ // créer un groupe
	// incrémente automatiquement l'id et fait le lien entre le responsable et le nouveau groupe
	require ("modele/connect.php") ;  
	$sql='INSERT INTO groupe (nomGrp, IdUserResponsable, IdEve, nomEve, dateEve, adresseEve) VALUES(?, ?, ?, ?, ?, ?)';

	try {
		$commande = $pdo->prepare($sql);
		$bool = $commande->execute(array($NomGroupe, $idUserResponsable, $idEve, $NomEve, $dateEve, $adresseEve));
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
	$lastid = getLastIdGroupe();
	créerLien($lastid[0]['MAX(IdGrp)'],$idUserResponsable, 'ACCEPTER');
}

function créerLien($idGrp, $idUser, $enum) { //créer un lien entre un groupe et un utilisateur
	require ("modele/connect.php") ;  

	$sql='INSERT INTO assGrpUser (IdGrp, IdUser, InvStatut) VALUES(?, ?, ?)';

	try {
		$commande = $pdo->prepare($sql);
		$bool = $commande->execute(array($idGrp,  $idUser, $enum));
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
}

/* Fonction appellé pour supprimer l'invitation ou la mettre à jour */
function modifInvitationStatut($idGrp, $idUser, $boolVal) { 
	require ("modele/connect.php") ;  
	if($boolVal == 0) {
		$sql='DELETE FROM assGrpUser WHERE IdGrp=:idGrp AND IdUser=:idUser';
		try {
			$commande = $pdo->prepare($sql);
			$commande->bindParam(':idGrp', $idGrp);
			$commande->bindParam(':idUser', $idUser);
			$bool = $commande->execute();
			if($bool) {
				return true;
			} else {
				return false;
			}			
		}
		catch (PDOException $e) {
			echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
			die(); // On arrête tout.
		}
	}
	else if($boolVal == 1){
		$sql='UPDATE assGrpUser SET InvStatut=:invStatut WHERE IdGrp=:idGrp AND IdUser=:idUser ';
		$invStatut = (String)'ACCEPTER';
		try {
			$commande = $pdo->prepare($sql);
			$commande->bindParam(':idGrp', $idGrp);
			$commande->bindParam(':idUser', $idUser);
			$commande->bindParam(':invStatut', $invStatut);
			$bool = $commande->execute();
			if($bool) {
				return true;
			} else {
				return false;
			}	
		}
		catch (PDOException $e) {
			echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
			die(); // On arrête tout.
		}
	}	
}


function getNbInvFromId($idUser) {
	require ("modele/connect.php") ;  

	$sql='SELECT COUNT(*) AS total FROM assGrpUser WHERE InvStatut =:invStatut AND IdUser=:idUser';
	$invStatus = (String)'ATTENTE';
	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':invStatut', $invStatus);
		$commande->bindParam(':idUser', $idUser);
		$bool = $commande->execute();
		if ($bool) {
			$resultat = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			return $resultat;
		}
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
}


function getUserIdFromName($nom) { // return l'id d'un utilisateur avec son login
	require ("modele/connect.php") ; 

	$sql="SELECT IdUser FROM utilisateur WHERE nomUtilisateur=:nom";
	$resultat= array(); 

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':nom', $nom);
		$bool = $commande->execute();
		

		if ($bool) {
			$resultat = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			return $resultat;
		}
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
}

function getLastIdGroupe() {
	require("modele/connect.php");
	$sql="SELECT MAX(IdGrp) FROM groupe";
	$resultat= array(); 

	try {
		$commande = $pdo->prepare($sql);
		$bool = $commande->execute();
		

		if ($bool) {
			$resultat = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
			return $resultat;
		}
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
}

function getInvitInfoFromId($id){
	require ("modele/connect.php");
	$sql="SELECT Idgrp FROM assGrpUser WHERE IdUser =:IdUser AND InvStatut=:invStatut";
	$resultat= array(); 
	$invStatut="ATTENTE";
	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':IdUser', $id);
		$commande->bindParam(':invStatut', $invStatut);
		$bool = $commande->execute();
		if ($bool) {
			$resultat = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
		}
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
	$cpt = 0;
	$tabRes= array();
	foreach($resultat as $IdGrp) {
		$TrueIdGrp = (int)$IdGrp['Idgrp'];
		$sql1="SELECT IdGrp, nomGrp, nomEve, dateEve, adresseEve FROM groupe WHERE IdGrp=:IdGrp";
		$resultat1= array(); 
		
		$sql2="SELECT nomUtilisateur FROM assGrpUser INNER JOIN utilisateur ON assGrpUser.IdUser = utilisateur.IdUser WHERE IdGrp=:IdGrp And InvStatut=:invStatut";
		$resultat2= array();
		$confirm = "ACCEPTER";
		try {
			$commande1 = $pdo->prepare($sql1);
			$commande1->bindParam(':IdGrp', $TrueIdGrp);
			$bool1 = $commande1->execute();
			$commande2 = $pdo->prepare($sql2);
			$commande2->bindParam(':IdGrp', $TrueIdGrp);
			$commande2->bindParam(':invStatut', $confirm);
			$bool2 = $commande2->execute();

			if ($bool1) {
				$resultat1 = $commande1->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements des infos du groupe
				if($bool2) {
					$resultat2 = $commande2->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements du nombre de participants				
					$tabRes[$cpt][0] = $resultat1;
					$tabRes[$cpt][1] = $resultat2;
					$cpt++;
				}
			}
		} catch (PDOException $e) {
			echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
			die(); // On arrête tout.
		}	
	}	
	return $tabRes;
}

function getGrpFromId($id) { //return le nombre de participant, les participants et le groupe
	require ("modele/connect.php") ; 

	$sql="SELECT IdGrp FROM assGrpUser WHERE IdUser =:IdUser AND InvStatut=:invStatut";
	$resultat= array(); 
	$invStatut="ACCEPTER";

	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':IdUser', $id);
		$commande->bindParam(':invStatut', $invStatut);
		$bool = $commande->execute();
		if ($bool) {
			$resultat = $commande->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements
		}
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}	
	$cpt = 0;
	$tabRes= array();
	foreach($resultat as $IdGrp) {

		$TrueIdGrp = (int)$IdGrp['IdGrp'];
		$sql1="SELECT nomGrp, nomEve, dateEve, adresseEve FROM groupe WHERE IdGrp=:IdGrp";
		$resultat1= array(); 

		$sql2="SELECT COUNT(IdUser) as total FROM assGrpUser WHERE IdGrp=:IdGrp AND InvStatut=:invStatut";
		$resultat2= array(); 

		$sql3="SELECT nomUtilisateur FROM assGrpUser INNER JOIN utilisateur ON assGrpUser.IdUser = utilisateur.IdUser WHERE IdGrp=:IdGrp AND InvStatut=:invStatut";
		$resultat3= array();

		try {
			$commande1 = $pdo->prepare($sql1);
			$commande1->bindParam(':IdGrp', $TrueIdGrp);
			$bool1 = $commande1->execute();
			$commande2 = $pdo->prepare($sql2);
			$commande2->bindParam(':IdGrp', $TrueIdGrp);
			$commande2->bindParam(':invStatut', $invStatut);
			$bool2 = $commande2->execute();
			$commande3 = $pdo->prepare($sql3);
			$commande3->bindParam(':IdGrp', $TrueIdGrp);
			$commande3->bindParam(':invStatut', $invStatut);
			$bool3 = $commande3->execute();
			if ($bool1) {
				$resultat1 = $commande1->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements des infos du groupe
				if($bool2) {
					$resultat2 = $commande2->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements du nombre de participants
					if($bool3) {
						$resultat3 = $commande3->fetchAll(PDO::FETCH_ASSOC); //tableau d'enregistrements des noms utilisateurs						
						$tabRes[$cpt][0] = $resultat1;
						$tabRes[$cpt][1] = $resultat2[0];
						$tabRes[$cpt][2] = $resultat3;
						$cpt++;
					}
				}
		
			}
		} catch (PDOException $e) {
			echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
			die(); // On arrête tout.
		}	
	}
	return $tabRes;
}

?>