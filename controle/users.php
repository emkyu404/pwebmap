<?php 

function ident() {
	$login=  isset($_POST['login'])?($_POST['login']):'';
	$mdp =  isset($_POST['mdp'])?($_POST['mdp']):'';
	$msg='';

	if  (count($_POST)==0)
    	require ("./vue/users/ident.tpl");
    else {
	    if  (!verif_ident($login,$mdp)) {
			$_SESSION['login']=array();
			$_SESSION['mdp']="";
	        $msg ="Erreur : Login / Mot de passe incorrect";
	        require ("./vue/users/ident.tpl");
		}
	    else {
			$_SESSION['login'] = $login;
			$_SESSION['mdp'] = $mdp;
			$url = "index.php?controle=users&action=accueil";
			header ("Location:" .$url);	 
		}
    }	
}

function verif_ident($login,$mdp) {
	require_once ('./modele/usersBD.php');
	return verif_ident_BD($login,$mdp); //true ou false;
}

function inscription(){
	$login=  isset($_POST['login'])?($_POST['login']):'';
	$mdp =  isset($_POST['mdp'])?($_POST['mdp']):'';
	$confirmMdp = isset($_POST['confirmMdp'])?($_POST['confirmMdp']):'';
	$msg='';

	if  (count($_POST)==0)
    	require ("./vue/users/ident.tpl");
    else {
	    if  (!verif_inscription($login,$mdp)) {
			$_SESSION['login']=array();
			$_SESSION['mdp']="";
	        $msg ="Erreur : Login / Mot de passe deja utilisé";
	        require ("./vue/users/ident.tpl");
		}
	    else {
	    	if($mdp == $confirmMdp){
	    		inscrire($login, $mdp);
				$msg = "Inscription reussie";
				require("./vue/users/ident.tpl");
	    	}
	    	else{
				$msg = "Erreur : Votre mot de passe et sa confirmation ne sont pas identiques";
				require("./vue/users/ident.tpl");
	    	}
			
		}
    }	
}

function verif_inscription($login,$mdp){
	require_once('./modele/usersBD.php');
	return verif_inscriptionBD($login,$mdp);
}

function inscrire($login,$mdp){
	require_once('./modele/usersBD.php');
	inscrireBD($login,$mdp);
}

function disconnect(){
	require('index.php');
}

function updateInvStatus(){
	$idGrp = $_POST['groupeID'];
	$idUser = $_SESSION['ID'];
	if((int)$_POST['invStatut'] == 1){
		$boolVal=1;
	}else{
		$boolVal=0;
	}
	require("./modele/usersBD.php");
	modifInvitationStatut($idGrp, $idUser, $boolVal);
	invitation();

}

function accueil() {
	$login = $_SESSION['login'];
	$mdp = $_SESSION['mdp'];
	require_once("./modele/usersBD.php");
	$idUser =(int) $_SESSION['ID'];
	$invitationNb = getNbInvFromId($idUser)[0]['total'];
	require("./vue/users/accueil.tpl");

}

function PageCreationGroupe() {
	require("./vue/users/creationGroupe.tpl");
}

function invitation(){
	require_once("./modele/usersBD.php");
	$id= (int) $_SESSION['ID'];
	$tabInv = getInvitInfoFromId($id);
	require("./vue/users/invitations.tpl");
}

function groupe(){
	require_once("./modele/usersBD.php");
	$id= (int) $_SESSION['ID'];
	$tabGroupe = getGrpFromId($id);
	require("./vue/users/groupes.tpl");
}

function creationGroupe() {
	require_once('./modele/usersBD.php');
	$nomGroupe = $_POST['nomGroupe'];
	$nomInvite = $_POST['nomInvite'];
	$idEve = $_POST['eventID'];
	$idResponsableUser = $_SESSION['ID'];
	$adresseEve = $_POST['addressEvent'];
	$dateEve = $_POST['dateEvent'];
	$nomEve = $_POST['nomEvent'];
	// les tests à faire :
	// tester si le nom du groupe n'est pas nul ou composé d'espace
	// c'est tout je crois omg
	
	créerGroupe($nomGroupe, $idResponsableUser, $idEve, $nomEve, $dateEve, $adresseEve);

	$tabInvite = array();
	$tabInvite = explode(";",$nomInvite); // contient tout les invités

	require_once('./modele/usersBD.php');
	InviterUtilisateur($tabInvite);
}
?>