<!doctype HTML>

<html>
    <head>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" href="./vue/stylesheet/ident.css">
        <title>Identification</title>
    </head>

    <body>
        <video id="bgvid" playsinline autoplay muted loop>
            <source src="./vue/resources/videos/indexBGloop.mp4" type="video/mp4">
        </video>   
        <div class="login-wrap">
            <div class="login-html">
                <img  id="parisLogo" src="./vue/resources/images/logoParis.png">
                <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Connexion</label>
                <input id="tab-2" type="radio" name="tab" class="for-pwd"><label for="tab-2" class="tab"> Inscription </label>
                <div class="login-form">
                    <div class="sign-in-htm">
                        <form method="post" action="index.php?controle=users&action=ident">
                            <div class="group">
                                <label for="user" class="label">Nom d'utilisateur</label>
                                <input id="login" type="text" class="input" name="login" required value="<?php echo $login; ?>">
                            </div>
                            <div class="group">
                                <label for="pass" class="label">Mot de passe</label>
                                <input id="password" type="password" class="input" required data-type="password", name="mdp", value="<?php echo $mdp; ?>">
                            </div>

                            <div class="group">
                                <input type="submit" class="button" value="se connecter">
                            </div>
                            <div class="hr"></div>
                        </form>
                    </div>

                    <div class="for-pwd-htm">
                        <form method="post" action="index.php?controle=users&action=inscription">
                            <div class="group">
                                <label for="user" class="label"> Nom d'utilisateur </label>
                                <input id="login" type="text" class="input" required name="login" value="<?php echo $login; ?>">
                            </div>

                            <div class="group">
                                <label for="user" class="label"> Mot de passe </label>
                                <input id="pass" type="password" class="input" required name="mdp" value="<?php echo $mdp; ?>">
                            </div>

                            <div class="group">
                                <label for="user" class="label"> Confirmation du Mot de passe</label>
                                <input id="confirmMdp" type="password"  required class="input" name="confirmMdp">
                            </div>
                            <div class="group">
                                <input type="submit" class="button" value="s'inscrire">
                            </div>
                            <div class="hr"></div>
                            </div>
                        </form>
                    </div>
                    <div>
                    <?php echo "<p id='errorMsg' style='color:red;'>".$msg."</p>"; ?>
                    </div>
                </div> 
            </div>
        </div>
    </body>

</html>