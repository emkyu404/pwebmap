<html>
    <head>
        <link rel="stylesheet" href="./vue/stylesheet/creationGroupe.css">
        <link rel="stylesheet" href="./vue/stylesheet/swal.css">
        <script src="./vue/scriptsJS/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="./vue/scriptsJS/creationGroupe.js"></script>
        <script src="https://kit.fontawesome.com/59f818b046.js" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <title> Création de groupe </title>
    </head>
    <body>

        <div class="notification-container">
                <div class="notification-icon"><p><img src="./vue/resources/images/back-icon.png"></p></div>
        </div>
        <div class="left form flex">
            <h1>CRÉER VOTRE PROPRE GROUPE !</h1>
            <form class="createGroupForm" action="index.php?controle=users&action=creationGroupe" method="POST">
                <label class="group-label-text" for="nomGroupe">NOM DU GROUPE</label>
                <input class="input input-text not-disabled" type="text" required name="nomGroupe" id="nomGroupe">
                <br>
                

                <i class="fas fa-lock icon"></i>
                <label class="group-label-text" for="nomEvent" >INTITULÉ DE L'ÉVÈNEMENT</label>
                <?php 
                    $eventSelect = $_POST["eventSelect"];
                    echo'<input disable readonly type="text" class="input input-text disabled" id="nomEvent" name="nomEvent" value="';
                    echo $eventSelect;
                    echo'"></center>' ;
                ?>
                <br>

                <i class="fas fa-lock icon"></i>
                <label class="group-label-text" for="dateEvent" >DATE DE L'ÉVÈNEMENT</label>
                <?php 
                    $dateEvent = $_POST["dateSelect"];
                    echo'<input disable readonly type="text" class="input input-text disabled" id="dateEvent" name="dateEvent" value="';
                    echo $dateEvent;
                    echo'"></center>' ;
                ?>
                <br>

                <i class="fas fa-lock icon"></i>
                <label class="group-label-text" for="dateEvent" >ADRESSE</label>
                <?php 
                    $addressEvent = $_POST["eventAddress"];
                    echo'<input disable readonly type="text" class="input input-text disabled" id="addressEvent" name="addressEvent" value="';
                    echo $addressEvent;
                    echo'"></center>' ;
                ?>
                <br>



                    <label class="group-label-text " for="nomInvite" >QUI SOUHAITEZ-VOUS AJOUTER ?</label>
                    <input type="text"  class="input input-text not-disabled" id="nomInvite">

                <br>
                <div class="flex row space-around">
                    <input class="input bouton semi-bouton" type="button" id="ajouter" value="AJOUTER"><input class="input bouton semi-bouton" id="annuler" type="button" value="RÉINITIALISER">
                </div>
                <br>
                <i class="fas fa-lock icon"></i>
                <label class="group-label-text" for="nomInvite" >PERSONNES INVITÉES</label>
                <input type="text"  readonly required class="input input-text disabled" id="invite" name="nomInvite">
                    
                <br>

                <?php
                    $eventID = $_POST["eventID"];
                    echo' <input type="hidden" id="eventID" name="eventID" value="';
                    echo $eventID;
                    echo'">';
                ?>

                <?php
                    $currentUSERLOG = $_SESSION["login"];
                    echo' <input type="hidden" id="currentLog" name="currentLog" value="';
                    echo $currentUSERLOG;
                    echo'">';
                ?>

                <?php
                    $currentUSERID = $_SESSION["ID"];
                    echo' <input type="hidden" id="currentID" name="currentID" value="';
                    echo $currentUSERID;
                    echo'">';
                ?>

                
                <input id="submit" class="input bouton" type="submit" value="CRÉATION DU GROUPE">
                <div class="hr"></div>
            </form>
        </div>

        <div class="imageContainer">
            <?php
                $eventCoverURL = $_POST["eventCoverURL"];
                echo '<img id="background-img" src="';
                echo $eventCoverURL;
                echo '">';
            ?>
        </div>
    </body>
</html>