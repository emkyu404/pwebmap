<!doctype HTML>

<html>
    <head>
        <link rel="stylesheet" href="./vue/stylesheet/groupes.css">
        <link rel="stylesheet" href="./vue/stylesheet/swal.css">
        <script src="./vue/scriptsJS/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="./vue/scriptsJS/groupe.js"></script>
        <script src="https://kit.fontawesome.com/59f818b046.js" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <title> Vos groupes </title>
    </head>
    <body>
        <header>
            <span>
             <div class="notification-container">
                <div class="notification-icon"><p><img src="./vue/resources/images/back-icon.png"></p></div>
             </div>

             <div class="title">
                <h1> Consulter vos groupes </h1>
             </div>
            </span>
        </header>

        <div class="background">
            <img src="./vue/resources/images/groupe-bg.png">
        </div>
        <div class="content">
            <div class="mainContainer">
            <?php 
            foreach($tabGroupe as $groupe){
                echo('<div class="itemContainer">');
                echo('<div>');
                echo('<h1>');
                echo $groupe[0][0]['nomGrp'];
                echo('</h1>');
                echo('</div>');

                echo('<div>');
                echo('<p> Événement : '); echo $groupe[0][0]['nomEve']; echo('</p>');
                echo('<p> Date : '); echo $groupe[0][0]['dateEve']; echo('</p>');
                echo('<p> Adresse : '); echo $groupe[0][0]['adresseEve']; echo('</p>');
                echo('<p> Nombre de participants : '); echo $groupe[1]['total']; echo('</p>');
                echo('<p> Participant(s) : '); 
                foreach($groupe[2] as $participant){
                    echo $participant['nomUtilisateur'];
                    echo(' ');
                }
                echo('</p>');
                echo('</div>');
                echo('</div>');
            }
            ?>
        </div>
    </body>
</html>