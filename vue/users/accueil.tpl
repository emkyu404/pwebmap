<!DOCTYPE HTML>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

         <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="">
         <link rel="stylesheet" href="./vue/stylesheet/accueil.css">
         <link rel="stylesheet" href="./vue/stylesheet/swal.css">
         <link rel="stylesheet" href="./vue/stylesheet/jquery-ui.min.css">
          <script src="./vue/scriptsJS/jquery-3.4.1.min.js" type="text/javascript"></script>
         <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
         <script src="./vue/scriptsJS/map.js" type="text/javascript"></script>
         <script src="./vue/scriptsJS/data.js" type="text/javascript"></script>
         <script src="./vue/scriptsJS/accueil.js" type="text/javascript"></script>
         <script src="./vue/scriptsJS/jquery-ui.min.js" type="text/javascript"></script>
         <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        

        <title>Carte intéractive</title>
    </head>

    <body onload="initMap()">
        <div class="notification-container">
            <div id="invitations-number" class="notification-number">
                <p><?php echo $invitationNb; ?></p>
            </div>
            <div id="invitations" class="notification-icon"><p><img src="./vue/resources/images/mail.png"></p></div>
        </div>

        <div class="menu-container">
            <div id="group" class="menu-icon"><p><img src="./vue/resources/images/group-icon.png"></p></div>
            <div id="log-out" class="menu-icon"><p><img src="./vue/resources/images/log-out.png"></p></div>
        </div>
        <div class="mainContainer">
            <div id="nav" class="mainItemContainer">
                <div id="searchEngine" class="fixedItemContainer">
                    <form class="searchEngineForm" action="index.php?controle=users&action=PageCreationGroupe" method="POST">
                        <legend> Parcourez la liste des évenements à faire dans Paris </legend>
                        <label> Évenements </label>
                        <select class="dropdownEventSelect" name="eventSelect">
                        <option disabled selected value> -- Sélectionnez un évenement -- </option>
                        </select>

                        <label> Catégories </label>
                        <select class="dropdownTagSelect" name="tagSelect">
                        <option disabled selected value> -- Sélectionnez un mot-clé -- </option>
                        <option></option>
                        </select>
                        <div>
                        <label> Uniquement gratuit</label>
                        <input type="checkbox" class="cbPriceTypeSelect" name="priceTypeSelect">

                        <input type="hidden" id="eventID" name="eventID" value="">
                        <input type="hidden" id="eventCoverURL" name="eventCoverURL" value="">
                        <input type="hidden" id="eventAddress" name="eventAddress" value="">

                        <div id="date">
                            <label> Date : </label>
                            <input type="date"  required="required" disable onkeydown="return false" class="dateSelect" name="dateSelect" value="" min="" max="">
                        </div>
                        </div>
                        <input class="submitForm" type="button" value="Créer un groupe !">
                    </form>
                </div>

                <div class="content-wrapper">
                    <div id="eventInformations" class="overflowContainer">
                        <div class="overflowContent">
                            <a target="_blank" id="eventUrl" href="" >
                                <img id="eventCover" src="">
                            </a>
                            <h1 id="eventTitle"></h1>
                            <p id="eventLeadText"></p>
                            <div id="tabs">
                                <ul class="nav-tabs">
                                    <li><a href="#tabs-1"> Adresse </a></li>
                                    <li><a href="#tabs-2"> Date </a></li>
                                    <li><a href="#tabs-3"> Prix </a></li>
                                    <li><a href="#tabs-4"> Description</a></li>
                                    <li><a href="#tabs-5"> Contact </a></li>
                                </ul>
                                <div id="tabs-1" class="tabs-content">
                                    <p id="eventAddressName"></p>
                                    <p id="eventAddressStreet"></p>
                                </div>
                                <div id="tabs-2"  class="tabs-content">
                                    <p id="eventDate"></p>
                                </div>
                                <div id="tabs-3"  class="tabs-content">
                                    <p id="eventPriceType"></p>
                                    <p id="eventPrice"></p>
                                    <p id="eventAccessType"></p>
                                </div>
                                <div id="tabs-4"  class="tabs-content">
                                    <p id="eventDescription"></p>
                                </div>
                                <div id="tabs-5"  class="tabs-content">
                                    <p id="eventContactName"></p>
                                    <p id="eventContactPhone"></p>
                                    <p id="eventContactMail"></p>
                                    <p id="eventContactURL"></p>
                                    <p id="eventContactFacebook"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="mapid" class="mainItemContainer" ></div>
        </div>
    </body>

</html>