<!doctype HTML>

<html>
    <head>
        <link rel="stylesheet" href="./vue/stylesheet/invitations.css">
        <link rel="stylesheet" href="./vue/stylesheet/swal.css">
        <script src="./vue/scriptsJS/jquery-3.4.1.min.js" type="text/javascript"></script>
        <script src="./vue/scriptsJS/invitations.js"></script>
        <script src="https://kit.fontawesome.com/59f818b046.js" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <title> Vos invitations </title>
    </head>
    <body>
        <header>
            <span>
             <div class="notification-container">
                <div class="notification-icon"><p><img src="./vue/resources/images/back-icon.png"></p></div>
             </div>

             <div class="title">
                <h1> Consulter vos invitations </h1>
             </div>
            </span>
        </header>
        <div class="background">
            <img src="./vue/resources/images/invitation-bg.png">
        </div>
        <div class="content">
            <div class="mainContainer">
                <?php 
                foreach($tabInv as $invitation){
                    echo('<div class="itemContainer">');
                    echo('<div>');
                    echo('<h1> Vous avez été invité(e) à rejoindre le groupe "');
                    echo $invitation[0][0]['nomGrp'];
            
                    echo('"</h1>');
                    echo('</div>');

                    echo('<div>');
                    echo('<p> Événement : '); echo $invitation[0][0]['nomEve']; echo('</p>');
                    echo('<p> Date : '); echo $invitation[0][0]['dateEve']; echo('</p>');
                    echo('<p> Adresse : '); echo $invitation[0][0]['adresseEve']; echo('</p>');
                    echo('<p> Participant(s) : '); 
                    foreach($invitation[1] as $participant){
                        echo $participant['nomUtilisateur'];
                        echo(' ');
                    }
                    echo('</p>');
                    echo('</div>');
                    echo('<form class="invit-submit" method="post" action="index.php?controle=users&action=updateInvStatus">');
                    echo('<input type="text" name="groupeID" hidden value="');
                    echo $invitation[0][0]['IdGrp'];
                    echo('">');
                    echo('<input type="text" class="inv-statut" name="invStatut" hidden value="0">');
                    echo('<input type="button" class="submit-button" value="Repondre">');
                    echo('</form>');
                    echo('</div>');
                }
                ?>
            </div>
        </div>
    </body>
</html>