$(document).ready(function() {
    $("#tabs").tabs();

    $(".submitForm").click(function(){
        if(allFine()){
            $(".searchEngineForm").submit();
        } else{
            return;
        }
    });

    $("#log-out").click(function(){
        swal({
            title: "Déconnexion ?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                window.location.href = 'index.php';
            } else {
                return;
            }
          });
    })

    if($("#invitations-number>p:eq(0)").html() == "0"){
         $("#invitations-number>p:eq(0)").hide();
    } else if(parseInt($("#invitations-number>p:eq(0)").html()) > 9){
        $("#invitations-number>p:eq(0)").html("9+") ;
    }

    $("#invitations").click(function(){
        if($("#invitations-number>p:eq(0)").html() !=  "0"){
            window.location.href='index.php?controle=users&action=invitation';
        }
        else{
            swal("Hop hop hop", "Vous n'avez reçu aucune nouvelle invitation", "error");
        }
    });

    $("#group").click(function(){
        window.location.href='index.php?controle=users&action=groupe';
    });


});


function allFine(){
    if($(".dropdownEventSelect").val() == null){
        swal("Erreur", "Veuillez sélectionner un événement", "error");
        return false;
    }

    if(pastEvent($(".dateSelect").val()))   {
        swal("Erreur", "La date sélectionnée est invalide", "error");
        return false;
    }

    if($("#eventID").val() == null || $("#eventID").val() == ""){
        swal("Erreur", "Veuillez sélectionner à nouveau votre événement", "error");
        return false;
    }
    return true;
}