$(document).ready(function() {   
    $(".notification-icon").click(function(){
        swal({
            title: "Retour à la carte ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href='index.php?controle=users&action=accueil';
            } else {
                return;
            }
        });
    });

    $(".submit-button").click(function(){
        swal({
            title: "Que souhaitez-vous faire ?",
            icon: "info",
            closeOnClickOutside: false,
            buttons:{
                cancel:{
                    text:"Refuser",
                    value:false,
                    visible: true,
                    closeModal:true
                },
                confirm:{
                    text:"Accepter",
                    value:true,
                    visible: true,
                    closeModal:true
                }
            }
            
        })
        .then((result) => {
            if (result) {
                $(".inv-statut").val("1");
                swal({
                    title: "Vous avez accepter l'invitation",
                    icon:"success",
                    buttons:{
                        confirm:{
                            text:"OK",
                            value:true,
                            visible:true,
                            closeModal : true
                        }
                    }
                })
                .then((result) =>{
                    var form = $(this).parents('form:first');
                    form.submit();
                })
            } else {
                $(".inv-statut").val("0");
                swal({
                    title: "Vous avez refuser l'invitation",
                    icon:"success"
                })
                .then((result) =>{
                    var form = $(this).parents('form:first');
                    form.submit();
                })
            }
        });
    });
});