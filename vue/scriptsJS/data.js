var events = new Array();
var currentDate;
var conformEvent = new Array();
var selectvalue;
var tagsValues = new Array();
var selectTag;
var selectEvent;
var onlyFreeEvents = false;
var APIUrl = 'https://opendata.paris.fr/api/records/1.0/search/?dataset=que-faire-a-paris-&facet=category&facet=tags&facet=address_zipcode&facet=address_city&facet=pmr&facet=blind&facet=deaf&facet=access_type&facet=price_type'

$(document).ready(function(){

    currentDate = new Date();
    console.log("yo");

    $.ajax({
        url: APIUrl,
        datatype:"json",
        success: function(data){
            events = data.records;
        }
    })
    $.getJSON("./vue/resources/data/que-faire-a-paris-.json",function(data){
        events = events.concat(data);

        $.each(events, function(key, value){
            if(value.fields.date_end != null && pastEvent(value.fields.date_end) == false){
                if(value.fields.lat_lon != null && value.fields.url != null){
                    conformEvent.push(value);
                    $(".dropdownEventSelect").append($('<option>').text(value.fields.title).attr('value', value.fields.title));
                }
            }
        });


        $.each(conformEvent, function(key, value){
            if(value.fields.tags != null){
                var tabTags = wrapTags(value.fields.tags);
                $.each(tabTags, function(id, tag){
                    if(!isAlreadyIncluded(tag)){
                        tagsValues.push(tag);
                        $(".dropdownTagSelect").append($('<option>').text(tag).attr('value', tag));
                    }
                });
            }
        })
    });


    $(".dropdownEventSelect").change(function(){
        selectvalue = $(this).val()
        if(selectvalue == null){
            hideEventInformation();
        } else{
            $.each(conformEvent,function(key, value){
                if(value.fields.title === selectvalue){
                    selectEvent = value.fields;
                    updateEventInfo(value.fields);
                    setViewOnSelectedArea(value.fields.lat_lon[0], value.fields.lat_lon[1]);
                    return;
                }
            });
        }
    });

    $(".dropdownTagSelect").change(function(){
        selectTag = $(this).val();
        var noTagSelect = (selectTag === "");
        var first = true;
        $(".dropdownEventSelect").empty();
        $(".dropdownEventSelect").append($('<option>').text(" -- Sélectionnez un évenement -- ").attr('disabled','selected', 'value'));
        if(onlyFreeEvents){
            $.each(conformEvent, function(key, value){
                if(value.fields.tags != null && value.fields.lat_lon != null && value.fields.price_type.includes("gratuit")){
                    var tabTags = wrapTags(value.fields.tags);
                    if(noTagSelect || tabTags.includes(selectTag)){
                        $(".dropdownEventSelect").append($('<option>').text(value.fields.title).attr('value', value.fields.title));
                        if(first){
                            if(value.fields != null){
                                selectEvent = value.fields;
                                updateEventInfo(value.fields);
                                setViewOnSelectedArea(value.fields.lat_lon[0], value.fields.lat_lon[1]);
                                first = false;
                            }
                        }
                    }
                }
            });
        } else{
            $.each(conformEvent, function(key, value){
                if(value.fields.tags != null && value.fields.lat_lon != null){
                    var tabTags = wrapTags(value.fields.tags);
                    if(noTagSelect || tabTags.includes(selectTag)){
                        $(".dropdownEventSelect").append($('<option>').text(value.fields.title).attr('value', value.fields.title));
                        if(first){
                            if(value.fields != null){
                                selectEvent = value.fields;
                                updateEventInfo(value.fields);
                                setViewOnSelectedArea(value.fields.lat_lon[0], value.fields.lat_lon[1]);
                                first = false;
                            }
                        }
                    }
                }
            });
        }
    });

    $(".cbPriceTypeSelect").change(function(){
        $(".dropdownEventSelect").empty();
        var first = true;
        var noTagSelect = (selectTag === "");
        if(this.checked){
            onlyFreeEvents=true;
            $.each(conformEvent, function(key, value){
                if(value.fields.tags != null && value.fields.lat_lon != null && value.fields.price_type.includes("gratuit")){
                    var tabTags = wrapTags(value.fields.tags);
                    if(noTagSelect || tabTags.includes(selectTag)){
                        $(".dropdownEventSelect").append($('<option>').text(value.fields.title).attr('value', value.fields.title));
                        if(first){
                            if(value.fields != null){
                                selectEvent = value.fields;
                                updateEventInfo(value.fields);
                                setViewOnSelectedArea(value.fields.lat_lon[0], value.fields.lat_lon[1]);
                                first = false;
                            }
                        }
                    } 
                }
            });
        } else{
            onlyFreeEvents=false;
            $.each(conformEvent, function(key, value){
                if(value.fields.tags != null && value.fields.lat_lon != null){
                    var tabTags = wrapTags(value.fields.tags);
                    if(noTagSelect || tabTags.includes(selectTag)){
                        $(".dropdownEventSelect").append($('<option>').text(value.fields.title).attr('value', value.fields.title));
                        if(first){
                            if(value.fields != null){
                                selectEvent = value.fields;
                                updateEventInfo(value.fields);
                                setViewOnSelectedArea(value.fields.lat_lon[0], value.fields.lat_lon[1]);
                                first = false;
                            }
                        }
                    }
                }
            });
        }
    });

});


function wrapTags(tags){
    var tabTags;
    if(tags.includes(";")){
        tabTags = tags.split(";");
    } else{
        tabTags = new Array(1);
        tabTags[0] = tags;
    }
    return tabTags
}

function isAlreadyIncluded(tag){
    return tagsValues.includes(tag);
}

function updateEventInfo(event){

    if(event.id !=null){
        $("#eventID").val(event.id);
    } else{
        $("#eventID").val("");
    }
    if(event.title !=null){
        $("#eventTitle").html(event.title);
    } else{
        $("#eventTitle").empty();
    }
    
    if(event.lead_text !=null){
        $("#eventLeadText").html(event.lead_text);
    } else{
        $("#eventLeadText").empty();
    }

    if(event.cover_url !=null){
        $("#eventCover").attr("src", event.cover_url)
        $("#eventCoverURL").val(event.cover_url);
    } else{
        $("#eventCover").attr("src", "");
        $("#eventCoverURL").val();
    }

    if(event.url != null){
        $("#eventUrl").attr("href", event.url);
    }else{
        $("#eventUrl").attr("href", null);
    }

    if(event.address_name !=null){
        $("#eventAddressName").html(event.address_name);
    } else{
        $("#eventAddressName").empty();
    }

    if(event.address_street !=null){
        $("#eventAddressStreet").html(event.address_street + ", "+ event.address_city);
        $("#eventAddress").val(event.address_street + ", "+ event.address_city);
        console.log("yo");
    } else{
        $("#eventAddressStreet").empty();
        $("#eventAddress").val("");
    }

    if(event.date_start !=null && event.date_end != null){
        if(interprateDate(event.date_start.substr(0,10)).includes(interprateDate(event.date_end.substr(0.10)))){
            $("#eventDate").html("Le " + interprateDate(event.date_start));
            $(".dateSelect").attr("max" , event.date_start.substr(0,10));
            $(".dateSelect").attr("min" , event.date_start.substr(0,10));
            $(".dateSelect").val(event.date_start.substr(0,10));
        }else{
            $("#eventDate").html("Du " + interprateDate(event.date_start) + " au " + interprateDate(event.date_end));
            $(".dateSelect").attr("max" , event.date_end.substr(0,10));
            if(pastEvent(event.date_start.substr(0,10))){
                $(".dateSelect").val(getCurrentDate());
                $(".dateSelect").attr("min", getCurrentDate());
            } else{
                $(".dateSelect").val(event.date_start.substr(0,10));
                $(".dateSelect").attr("min" , event.date_start.substr(0,10));
            }
        }

    } else{
        $("#eventDate").empty();
    }

    if(event.price_type !=null){
        var catPrice = event.price_type;
        catPrice = replaceAt(catPrice,0, catPrice.charAt(0).toUpperCase());
        $("#eventPriceType").html("Catégorie : " + catPrice);
    } else{
        $("#eventPriceType").empty();
    }


    if(event.price_detail !=null){
        $("#eventPrice").html("Détails : " + event.price_detail);
    } else{
        $("#eventPrice").empty();
    }

    if(event.access_type !=null){
        var accType = event.access_type;
        accType = replaceAt(accType,0, accType.charAt(0).toUpperCase());
        $("#eventAccessType").html("Type d'accès : " + accType);
    } else{
        $("#eventAccessType").empty();
    }

    if(event.description !=null){
        $("#eventDescription").html(event.description);
    } else{
        $("#eventDescription").empty();
    }

    if(event.contact_name !=null){
        $("#eventContactName").html("Nom de contact : " + event.contact_name);
    } else{
        $("#eventContactName").empty();
    }

    if(event.contact_phone !=null){
        $("#eventContactPhone").html("Téléphone : " + event.contact_phone);
    } else{
        $("#eventContactPhone").empty();
    }

    if(event.contact_mail !=null){
        $("#eventContactMail").html("Mail : " + event.contact_mail);
    } else{
        $("#eventContactMail").empty();
    }

    if(event.contact_url !=null){
        $("#eventContactURL").html("Site Web : <a target='_blank' href='"+event.contact_url+"' >" + event.contact_url + "</a>");
    } else{
        $("#eventContactURL").empty();
    }

    if(event.contact_facebook !=null){
        $("#eventContactFacebook").html("Facebook : <a target='_blank' href='" + event.contact_facebook + "'>" + event.contact_facebook + "</a>");
    } else{
        $("#eventContactFacebook").empty();
    }

}

function replaceAt(string,index, replacement) {
    return string.substr(0, index) + replacement+ string.substr(index + replacement.length);
}


function getCurrentDate(){
    var jj = currentDate.getDate();
    if(jj < 10){
        jj = "0" + jj;
    }
    var mm = currentDate.getMonth() + 1;
    if(mm < 10){
        mm = "0" + mm;
    }
    var aaaa = currentDate.getFullYear();
    console.log(aaaa + "-" + mm + "-" +jj)
    return aaaa + "-" + mm + "-" +jj;
}

function updateEventInfoPopup(){
    var image;
    var title;
    var adress;
    var road;
    var url;

    //L'image dans la popup
    if(selectEvent.cover_url != null){
        image = selectEvent.cover_url;
    }
    else{
        image = null;
    }

    //Le titre dans la popup
    if(selectEvent.title != null){
        title = selectEvent.title;
    }
    else{
        title = null;
    }

    //L'adresse dans la popup
    if(selectEvent.address_name != null){
        adress = selectEvent.address_name;
    }
    else{
        adress = null;
    }

    if(selectEvent.address_street != null){
        road = selectEvent.address_street;
    }
    else{
        road = null;
    }

    if(selectEvent.url != null){
        url = selectEvent.url;
    }else{
        url = null;
    }

    var liste = [image, title, adress, road, url];
    return liste;
}

function interprateDate(eventDate){
    var year = eventDate.substring(0, 4);
    var monthNum = eventDate.substring(5, 7);
    var day = eventDate.substring(8,10);

    var month = whichMonth(monthNum);

    return day + " " + month + " " + year;
}


function whichMonth(month){
    switch(month){
        case '01' : return "Janvier";
        case '02' : return "Février";
        case '03' : return "Mars";
        case '04' : return "Avril";
        case '05' : return "Mai";
        case '06' : return "Juin";
        case '07' : return "Juillet";
        case '08' : return "Août";
        case '09' : return "Septembre";
        case '10' : return "Octobre";
        case '11' : return "Novembre";
        case '12' : return "Decembre";
    }
}

function pastEvent(eventDate){
    var year = eventDate.substring(0, 4);
    var month = eventDate.substring(5, 7);
    if(month.substring(0,1).includes("0")){
        month.substring(0,1).replace("0", "");
    }
    var day = eventDate.substring(8,10);
    if(day.substring(0,1).includes("0")){
        day.substring(0,1).replace("0", "");
    }
    if(parseInt(year) < parseInt(currentDate.getFullYear())){
        return true;
    }

    if(parseInt(month) < parseInt(currentDate.getMonth() + 1)){
        return true;
    }

    if(parseInt(day) < parseInt(currentDate.getDate())){
        return true;
    }
    return false;
}

function hideEventInformation(){
    var e = document.getElementById("eventInformations");
    e.style.display = "none";
}