$(document).ready(function() {   
    $(".notification-icon").click(function(){
        swal({
            title: "Retour à la carte ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href='index.php?controle=users&action=accueil';
            } else {
                return;
            }
        });
    });
});