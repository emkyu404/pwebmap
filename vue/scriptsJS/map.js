//window.onload() = initMap;

var map;
var popup;
var marker = new Array();



function initMap(){
    map = L.map('mapid').setView([48.841987, 2.268309], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
    }).addTo(map);

    popup = L.popup();
}

function setViewOnSelectedArea(lat, lng){
    map.setView([lat, lng], 13);
    marker.forEach(element => {
        map.removeLayer(element);
    });
    itemMarker = L.marker([lat,lng]).addTo(map);

    ListElementPopup = updateEventInfoPopup();

    var popupContent = '<a target="_blank" href="' + ListElementPopup[4] + '"><img src="' + ListElementPopup[0] + '"></a> <h2>' + ListElementPopup[1] + '</h2> <h4>' + ListElementPopup[2] + ', ' + ListElementPopup[3] + '</h4>';
    var popupOptions =
    {
      'className' : 'custom-popup'
    }
    itemMarker.bindPopup(popupContent, popupOptions).openPopup();
    marker.push(itemMarker);
}   
