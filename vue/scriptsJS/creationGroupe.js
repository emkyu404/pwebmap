

$(document).ready(function(){
    var userLogin = $("#currentLog").val();
    $("#ajouter").click(function() {
        var temp = $("#invite").val();
        if($("#nomInvite").val() == null || $("#nomInvite").val() == ""){
            swal("Erreur","Veuillez indiquer un utilisateur", "error");
        }
        if($("#nomInvite").val() == userLogin){
            swal("Erreur","Vous ne pouvez pas vous inviter vous même !", "error");
            $("#nomInvite").val("");
            return;
        }

        if(alreadyInvited($("#nomInvite").val(), $("#invite").val())){
            swal("Erreur","Utilisateur déjà ajouté", "error");
            return;
        }
        if(temp === ""){
            $("#invite").val(temp + $("#nomInvite").val());
            $("#nomInvite").val("");
        } else{
            $("#invite").val(temp +";" + $("#nomInvite").val());
            $("#nomInvite").val("");
        }
    });

    $("#annuler").click(function() {
        swal({
            title: "Réinitialisation de la liste d'invitation ?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                $("#invite").val("");
              swal("Liste réinitialisée", "", "success");
            } else {
                return;
            }
          });
    });

    $(".notification-icon").click(function(){
        swal({
            title: "Retour à la carte ",
            text: "Toute progression sera perdue",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                window.location.href='index.php?controle=users&action=accueil';
            } else {
                return;
            }
          });
    });
});

function alreadyInvited(login, allLogin){
    var res = false;
    if(allLogin != ""){
        var loginTab = allLogin.split(';');
        loginTab.forEach(element => {
            if(element==login){
                res = true;
            }
        });
    }
    return res;
}

alertConfig={                              
    boxBgColor:"red" // ou bien toute autre couleur #aaccee par exemple
};